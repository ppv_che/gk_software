package com.gk_software.training.task;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class POSTest {
    
    public SimplePOS pos;
    
    @Before
    public void beforeInitialization() {
        pos = new SimplePOS();
        
        List<Item> itemList = new ArrayList<Item>();
        
        for(int i = 0; i < 15; i++) {
            Item item = new Item(i, "testName", 95, "");
            itemList.add(item);
        }
        Item item = new Item(1, "testName", 95, "");
        itemList.add(item);
        pos.storeItems(itemList);
    }
    
    @Test
    public void testAvailavleSize() {
        Item item = new Item(1, "testName", 95, "");
        int countAvailable = pos.countAvailableItem(item);
        assertEquals(2, countAvailable);
    }
    
    @Test
    public void testAvailavleCount() {
        
        int countAvailable = pos.getListOfAvailbaleSaleItems().size();
        assertEquals(16, countAvailable);
    }
    
    @Test
    public void testAmountOfMoneyAfterSales() {
        pos.sellItem(new Item(3, "testName", 95, ""));
        pos.sellItem(new Item(10, "testName", 95, ""));
        pos.sellItem(new Item(11, "testName", 95, ""));
        
        int money = pos.getCurrentMoneyAmount();
        assertEquals(285, money);
        
        int countAvailable = pos.getListOfAvailbaleSaleItems().size();
        assertEquals(13, countAvailable);
    }
    
    @Test
    public void testAmountOfMoneyAfterReturn() {
        pos.sellItem(new Item(3, "testName", 95, ""));
        pos.sellItem(new Item(10, "testName", 95, ""));
        pos.sellItem(new Item(11, "testName", 95, ""));
        
        pos.returnSoldItem(new Item(3, "testName", 95, ""));
        
        int money = pos.getCurrentMoneyAmount();
        assertEquals(190, money);
        
        int countAvailable = pos.getListOfAvailbaleSaleItems().size();
        assertEquals(14, countAvailable);

    }
    
    @Test
    public void testEncashment() {
        pos.sellItem(new Item(3, "testName", 95, ""));
        pos.sellItem(new Item(10, "testName", 95, ""));
        pos.sellItem(new Item(11, "testName", 95, ""));
        
        pos.encashment();
        
        int money = pos.getCurrentMoneyAmount();
        assertEquals(100, money);
    }

}
