package com.gk_software.training.task;

import com.gk.training.task.comm.Callee;
import com.gk.training.task.comm.Caller;

public class CommPOS extends SimplePOS implements Caller {
    
    private Callee callee;
    
    public void setCallee(Callee callee) {
        this.callee = callee;        
    }

    public boolean sendMoneyToCallee(int money) {
        if (!this.callee.isBusy()) {
            int start = this.callee.getMoneyInputContainer();
            this.callee.updateMoneyInputContainer(money);
            int end = this.callee.getMoneyInputContainer();
            if ((end - start)==money) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public int getMoneyFromCalleeInputContainer() {
        return this.callee.getMoneyInputContainer();
    }

    public void removeMoneyFromCalleeInputContainer() {
        this.callee.removeMoneyInputContainer();
    }

    public boolean isCalleeBusy() {
        return this.callee.isBusy();
    }

}
